import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router';

const files = import.meta.globEager('./*.ts');
let routes: RouteRecordRaw[] = [];
Object.keys(files).forEach((name) => {
  routes = [...routes, ...files[name].default];
});

export default createRouter({
  history: createWebHistory(),
  routes
});
